#!/bin/bash

if which clang-format-3.8 > /dev/null; then
    CFORMAT=`which clang-format-3.8`
elif which clang-format-mp-3.8 > /dev/null; then
    CFORMAT=`which clang-format-mp-3.8`
else
    echo "Could not find clang format"
    exit 1
fi

SOURCE_DIRS="src lib python"

for dir in ${SOURCE_DIRS}; do
    echo "Reformatting in ${dir}"
    find ${dir} -type f -iregex ".*\.\(c\|cc\|cxx\|cpp\|h\|hh\|hpp\|hxx\)" \! -iname catch.hpp \! -iname sqlite3.\* \! -iname bash_pattern.\* -exec "${CFORMAT}" -i -style=file -fallback-style=none {} \;
done
